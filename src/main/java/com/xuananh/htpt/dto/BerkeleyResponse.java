package com.xuananh.htpt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BerkeleyResponse implements Serializable {
    private int P1;
    private int P2;
    private int P3;
    private String synchronizedTime;

}
