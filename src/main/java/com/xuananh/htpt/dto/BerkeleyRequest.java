package com.xuananh.htpt.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BerkeleyRequest implements Serializable {
    private String date1;
    private String date2;
    private String date3;
}
