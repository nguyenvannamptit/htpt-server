package com.xuananh.htpt.controller;

import com.xuananh.htpt.dto.BerkeleyRequest;
import com.xuananh.htpt.dto.BerkeleyResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin("*")
public class Controller {
    @PostMapping("/berkeley")
    public ResponseEntity<BerkeleyResponse> answer(@RequestBody BerkeleyRequest request) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        LocalDateTime P1 = LocalDateTime.parse(request.getDate1(), formatter);
        LocalDateTime P2 = LocalDateTime.parse(request.getDate2(), formatter);
        LocalDateTime P3 = LocalDateTime.parse(request.getDate3(), formatter);
        double between0 = 0;
        double between1 = ChronoUnit.MILLIS.between(P1, P2);
        double between2 = ChronoUnit.MILLIS.between(P1, P3);
        double result = (between0 + between1 + between2) / 3;
        double a0 = result - between0;
        double a1 = result - between1;
        double a2 = result - between2;
        System.out.println(result);
        System.out.println("Điều chỉnh (ms)");
        System.out.println((int) Math.round(a0));
        System.out.println((int) Math.round(a1));
        System.out.println((int) Math.round(a2));
        LocalDateTime a = P1.plusNanos(Math.round(result) * 10 * 10 * 10 * 10 * 10 * 10);
        System.out.println("Thời gian sau đồng bộ: " + a.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
        BerkeleyResponse response = BerkeleyResponse.builder()
                .P1((int) Math.round(a0))
                .P2((int) Math.round(a1))
                .P3((int) Math.round(a2))
                .synchronizedTime(a.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")))
                .build();
        return ResponseEntity.ok(response);
    }

}
